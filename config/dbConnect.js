const { default: mongoose } = require("mongoose")

// const dbConnect = () => {
// 	try {
// 		mongoose.connect(process.env.MONGODB_URI)
// 		console.log("Database Connected Successfully!")
// 	} catch (error) {
// 		console.log("Database error")
// 		throw new Error(error)
// 	}
// }
// module.exports = dbConnect

const { MongoClient, ServerApiVersion } = require('mongodb');

const dbConnect = async () => {
	const URI = process.env.MONGODB_ATLAS_URI;
	mongoose
		.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true })
		.then(() => {
			console.log('Connected to DB');
		})
		.catch((err) => {
			console.log('err', err);
		});
};

module.exports = dbConnect

