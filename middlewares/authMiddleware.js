const User = require("../models/userModel");
const jwt = require("jsonwebtoken");
const asyncHandle = require("express-async-handler");

const authMiddleware = asyncHandle(async (req, res, next) => {
  let token;
  if (req?.headers?.authorization?.startsWith("Bearer")) {
    token = req.headers.authorization.split(" ")[1];
    try {
      if (token) {
        const decode = jwt.verify(token, process.env.JWT_SECRET);
        const user = await User.findById(decode?.id);
        req.user = user;
        next();
      }
    } catch (error) {
      throw new Error("Not Authorization token expired, Please Login again!");
    }
  } else {
    throw new Error("There is no token attached to header");
  }
});

const isEmployee = asyncHandle(async (req, res, next) => {
  const { email } = req.user;
  const adminUser = await User.findOne({ email });
  if (adminUser.role === "user") {
    throw new Error("You are not an employee");
  } else {
    next();
  }
});

const isAdmin = asyncHandle(async (req, res, next) => {
  const { email } = req.user;
  const adminUser = await User.findOne({ email });
  if (adminUser.role !== "admin") {
    throw new Error("You are not an admin");
  } else {
    next();
  }
});

module.exports = { authMiddleware, isEmployee, isAdmin };
