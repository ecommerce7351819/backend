const mongoose = require("mongoose");

// Declare the Schema of the Mongo model
var productSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
    },
    slug: {
      type: String,
      required: true,
      unique: true,
      lowercase: true,
    },
    description: {},
    price: {
      type: Number,
      required: true,
    },
    discount: {
      type: Number,
    },
    category: [],
    brand: {
      type: String,
      require: true,
    },
    sold: {
      type: Number,
      default: 0,
    },
    mainImg: {},
    images: [],
    variants: [
      {
        color: {
          type: String,
          require: true,
        },
        quantity: {
          type: Number,
          require: true,
        },
      },
    ],
    specifications: [
      {
        name: {
          type: String,
          require: true,
        },
        content: {
          type: String,
          require: true,
        },
      },
    ],
    tags: [],
    ratings: [
      {
        star: Number,
        comment: String,
        postedby: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "User",
        },
        user: String,
      },
    ],
    totalRating: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
  }
);

//Export the model
module.exports = mongoose.model("Product", productSchema);
