const mongoose = require("mongoose");

// Declare the Schema of the Mongo model
var orderSchema = new mongoose.Schema(
  {
    cart: {},
    info: {},
    paymentIntent: {
      type: String,
      default: "Unpaid",
      enum: ["Unpaid", "Paid"],
    },
    orderStatus: {
      type: String,
      default: "Processing",
      enum: ["Processing", "Shipping", "Cancelled", "Completed"],
    },
    orderby: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  },
  {
    timestamps: true,
  }
);

//Export the model
module.exports = mongoose.model("Order", orderSchema);
