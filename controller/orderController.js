const asyncHandler = require("express-async-handler");
const Order = require("../models/orderModel");
const validateMongoDbId = require("../utils/validateMongodbId");
const Cart = require("../models/cartModel");

//Create Order
const createOrder = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validateMongoDbId(_id);
  try {
    const cart = await Cart.findOne({ orderby: _id })
      .populate({
        path: "products.product",
        select: "_id title mainImg",
      })
      .exec();
    await Order.create({
      cart: cart,
      info: req.body,
      paymentIntent: "Unpaid",
      orderby: _id,
    });
    res.json({
      message: "Success",
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Get All Order
const getAllOrder = asyncHandler(async (req, res) => {
  try {
    const page = parseInt(req.query.page) || 1;
    const size = parseInt(req.query.size) || 10;

    const skip = (page - 1) * size;

    const orders = await Order.find()
      .skip(skip)
      .limit(size)
      .sort({ createdAt: -1 })
      .populate({
        path: "cart.products.product",
        select: "_id title mainImg",
      })
      .exec();

    const count = await Order.countDocuments({});

    res.json({ orders, count });
  } catch (error) {}
});

//Get A Order
const getAOrder = asyncHandler(async (req, res) => {
  try {
  } catch (error) {}
});

//Get Orders by userId
const getOrdersByUserId = asyncHandler(async (req, res) => {
  let { id } = req.params;
  if (!id) {
    id = req.user;
  }
  validateMongoDbId(id);
  try {
    const page = parseInt(req.query.page) || 1;
    const size = parseInt(req.query.size) || 10;

    const skip = (page - 1) * size;

    const orders = await Order.find({ orderby: id })
      .skip(skip)
      .limit(size)
      .sort({ createdAt: -1 })
      .exec();
    const count = await Order.countDocuments({});

    res.json({ orders, count });
  } catch (error) {}
});

const changeOrderStatus = asyncHandler(async (req, res) => {
  try {
    const { id } = req.params;
    const order = await Order.findByIdAndUpdate(id, req.body);
    res.json(order);
  } catch (error) {}
});

const changeOrderPay = asyncHandler(async (req, res) => {
  try {
    const { id } = req.params;
    const order = await Order.findByIdAndUpdate(id, req.body);
    res.json(order);
  } catch (error) {}
});

module.exports = {
  createOrder,
  getAllOrder,
  getAOrder,
  getOrdersByUserId,
  changeOrderStatus,
  changeOrderPay,
};
