const Product = require("../models/productModel");
const asyncHandler = require("express-async-handler");
const User = require("../models/userModel");
const validateMongoDbId = require("../utils/validateMongodbId");

//Create Product
const createProduct = asyncHandler(async (req, res) => {
  try {
    await Product.create(req.body);
    res.json({
      message: "Success",
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Get a product
const getProduct = asyncHandler(async (req, res) => {
  const { slug } = req.params;
  try {
    const product = await Product.findOne({ slug: slug });
    res.json(product);
  } catch (error) {
    throw new Error(error);
  }
});

//Get all products
const getAllProducts = asyncHandler(async (req, res) => {
  try {
    //Filtering
    const queryObj = { ...req.query };
    const excludeFields = ["page", "sort", "limit", "fields", "keyword"];
    excludeFields.forEach((el) => delete queryObj[el]);

    let queryStr = JSON.stringify(queryObj);
    queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);

    let query = Product.find(JSON.parse(queryStr));

    //search
    if (req.query.keyword) {
      const keyword = req.query.keyword;
      const searchConditions = {
        $or: [
          { title: { $regex: keyword, $options: "i" } }, // Search by title
          { category: { $regex: keyword, $options: "i" } }, // Search by category
          { brand: { $regex: keyword, $options: "i" } }, // Search by brand
        ],
      };
      query = query.find(searchConditions);
    }

    //Sorting
    if (req.query.sort) {
      const [sortBy, sortOrder] = req.query.sort.split("-");
      const sortObject = { [sortBy]: sortOrder === "descending" ? -1 : 1 };
      query = query.sort(sortObject);
    } else {
      query = query.sort("-createdAt");
    }

    //Limiting the fields
    if (req.query.fields) {
      const fields = req.query.sort.split(",").join(" ");
      query = query.select(fields);
    } else {
      query = query.select("-__v");
    }

    //Pagination
    const page = req.query.page;
    let limit = req.query.limit;
    if (!limit) {
      limit = 12;
    }
    const skip = (page - 1) * limit;
    query = query.skip(skip).limit(limit);
    let productCount = 0;
    if (req.query.page) {
      productCount = await Product.countDocuments(queryObj);
      if (skip >= productCount) throw new Error("This page does not exists");
    }
    const products = await query;

    const data = [];
    products.map((i) => {
      data.push({
        _id: i._id,
        title: i.title,
        slug: i.slug,
        price: i.price,
        discount: i.discount,
        category: i.category,
        brand: i.brand,
        mainImg: i.mainImg,
        totalRating: i.totalRating,
        ratings: i.ratings,
      });
    });

    res.json({ data, productCount });
  } catch (error) {
    throw new Error(error);
  }
});

const getHotProducts = asyncHandler(async (req, res) => {
  try {
    const { category, count } = req.query;
    const query = { category: { $in: [category] } };
    const data = await Product.find(query);
    data.sort((a, b) => b.sold - a.sold);
    const topProducts = data.slice(0, count);
    const result = [];
    topProducts.map((i) => {
      result.push({
        _id: i._id,
        title: i.title,
        slug: i.slug,
        price: i.price,
        discount: i.discount,
        category: i.category,
        brand: i.brand,
        mainImg: i.mainImg,
        totalRating: i.totalRating,
        ratings: i.ratings,
      });
    });
    res.json(result);
  } catch (error) {
    throw new Error(error);
  }
});

//Update Product
const updateProduct = asyncHandler(async (req, res) => {
  try {
    const { id } = req.params;
    await Product.findOneAndUpdate({ _id: id }, req.body, {
      new: true,
    });
    res.json({
      message: "Done",
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Delete product
const deleteProduct = asyncHandler(async (req, res) => {
  try {
    const { id } = req.params;
    await Product.findByIdAndDelete(id);
    res.json({
      message: "Done",
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Add to wishlist
const addToWishlist = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { proId } = req.body;
  try {
    const user = await User.findById(_id);
    const alreadyAdded = user.wishlist.find((id) => id.toString() === proId);
    if (alreadyAdded) {
      let user = await User.findByIdAndUpdate(
        _id,
        {
          $pull: { wishlist: proId },
        },
        {
          new: true,
        }
      );
      res.json({ user, message: "Remove successfully" });
    } else {
      let user = await User.findByIdAndUpdate(
        _id,
        {
          $push: { wishlist: proId },
        },
        {
          new: true,
        }
      );
      res.json({ user, message: "Success" });
    }
  } catch (error) {
    throw new Error(error);
  }
});

//Rating
const rating = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { star, prodId, comment } = req.body;
  try {
    const product = await Product.findById(prodId);
    let alreadyRated = product?.ratings.find(
      (userId) => userId.postedby.toString() === _id.toString()
    );
    if (alreadyRated) {
      const updateRating = await Product.updateOne(
        {
          ratings: { $elemMatch: alreadyRated },
        },
        {
          $set: { "ratings.$.star": star, "ratings.$.comment": comment },
        },
        {
          new: true,
        }
      );
    } else {
      const rateProduct = await Product.findByIdAndUpdate(
        prodId,
        {
          $push: {
            ratings: {
              star: star,
              comment: comment,
              postedby: _id,
              user: req.user.firstName + " " + req.user.lastName,
            },
          },
        },
        {
          new: true,
        }
      );
    }
    const getAllRating = await Product.findById(prodId);
    let totalRating = getAllRating.ratings.length;
    let ratingSum = getAllRating.ratings
      .map((item) => item.star)
      .reduce((prev, curr) => prev + curr, 0);
    let actualRating = Math.round((ratingSum / totalRating) * 2) / 2;
    let finalProduct = await Product.findByIdAndUpdate(
      prodId,
      {
        totalRating: actualRating,
      },
      {
        new: true,
      }
    );
    res.json(finalProduct);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  createProduct,
  getProduct,
  getAllProducts,
  updateProduct,
  deleteProduct,
  addToWishlist,
  rating,
  getHotProducts,
};
