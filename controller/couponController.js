const Coupon = require("../models/couponModel");
const validateMongoDbId = require("../utils/validateMongodbId");
const asyncHandle = require("express-async-handler");

//Create a coupon
const createCoupon = asyncHandle(async (req, res) => {
  try {
    await Coupon.create(req.body);
    res.json({
      message: "Success",
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Get all coupon
const getAllCoupons = asyncHandle(async (req, res) => {
  try {
    const allCoupon = await Coupon.find();
    res.json(allCoupon);
  } catch (error) {
    throw new Error(error);
  }
});

//Update a coupon
const updateCoupon = asyncHandle(async (req, res) => {
  try {
    const { id } = req.params;
    validateMongoDbId(id);
    await Coupon.findByIdAndUpdate(id, req.body, { new: true });
    res.json({
      message: "Done",
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Get a coupon
const getCoupon = asyncHandle(async (req, res) => {
  try {
    const { id } = req.params;
    validateMongoDbId(id);
    const coupon = await Coupon.findById(id);
    res.json(coupon);
  } catch (error) {
    throw new Error(error);
  }
});

//Delete coupon
const deleteCoupon = asyncHandle(async (req, res) => {
  try {
    const { id } = req.params;
    validateMongoDbId(id);
    await Coupon.findByIdAndDelete(id);
    res.json({
      message: "Done",
    });
  } catch (error) {
    throw new Error(error);
  }
});

const checkCoupon = asyncHandle(async (req, res) => {
  try {
    const { coupon } = req.body;
    const check = await Coupon.findOne({ name: coupon });
    if (check) {
      const currentDate = new Date();
      if (check?.expiry >= currentDate) {
        res.json({ message: "Success", discount: check.discount });
      } else {
        res.json({ message: "Expires" });
      }
    } else {
      res.json({ message: "Not Found" });
    }
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  createCoupon,
  getAllCoupons,
  updateCoupon,
  getCoupon,
  deleteCoupon,
  checkCoupon,
};
