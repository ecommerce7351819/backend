const asyncHandle = require("express-async-handler");

const createPayment = asyncHandle(async (req, res, next) => {
  var ipAddr =
    req.headers["x-forwarded-for"] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    req.connection.socket.remoteAddress;

  var config = require("../config/configCheckout");
  //var dateFormat = require("dateformat");

  //https://sandbox.vnpayment.vn/paymentv2/vpcpay.html
  // ?vnp_Amount=1806000
  // &vnp_Command=pay
  // &vnp_CreateDate=20210801153333
  // &vnp_CurrCode=VND
  // &vnp_IpAddr=127.0.0.1
  // &vnp_Locale=vn
  // &vnp_OrderInfo=Thanh+toan+don+hang+%3A5
  // &vnp_OrderType=other
  // &vnp_ReturnUrl=https%3A%2F%2Fdomainmerchant.vn%2FReturnUrl
  // &vnp_TmnCode=DEMOV210
  // &vnp_TxnRef=5
  // &vnp_Version=2.1.0
  // &vnp_SecureHash=...

  var tmnCode = config.vnp_TmnCode;
  var secretKey = config.vnp_HashSecret;
  var vnpUrl = config.vnp_Url;
  var returnUrl = config.vnp_ReturnUrl;

  var date = new Date();

  var createDate = req.body.createAt;
  var orderId = req.body.orderId || 1231234;
  var amount = req.body.amount;
  var bankCode = "NCB";

  var orderInfo = "Thanh toan cho ma GD: ";
  var orderType = req.body.orderType;
  var locale = req.body.language;
  if (locale === null || locale === "") {
    locale = "vn";
  }
  var currCode = "VND";
  var vnp_Params = {};
  vnp_Params["vnp_Version"] = "2.1.0";
  vnp_Params["vnp_Command"] = "pay";
  vnp_Params["vnp_TmnCode"] = tmnCode;
  // vnp_Params['vnp_Merchant'] = ''
  vnp_Params["vnp_Locale"] = "vn";
  vnp_Params["vnp_CurrCode"] = currCode;
  vnp_Params["vnp_TxnRef"] = "orderId";
  vnp_Params["vnp_OrderInfo"] = orderInfo + orderId;
  vnp_Params["vnp_OrderType"] = orderType;
  vnp_Params["vnp_Amount"] = 10000 * 100;
  vnp_Params["vnp_ReturnUrl"] = returnUrl;
  vnp_Params["vnp_IpAddr"] = ipAddr;
  vnp_Params["vnp_CreateDate"] = "231026172200";
  if (bankCode !== null && bankCode !== "") {
    vnp_Params["vnp_BankCode"] = bankCode;
  }

  vnp_Params = sortObject(vnp_Params);

  var querystring = require("qs");
  var signData = querystring.stringify(vnp_Params, { encode: false });
  var crypto = require("crypto");
  var hmac = crypto.createHmac("sha512", secretKey);
  var signed = hmac.update(new Buffer(signData, "utf-8")).digest("hex");
  vnp_Params["vnp_SecureHash"] = signed;
  vnpUrl += "?" + querystring.stringify(vnp_Params, { encode: false });

  res.redirect(vnpUrl);
});

function sortObject(obj) {
  let sorted = {};
  let str = [];
  let key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) {
      str.push(encodeURIComponent(key));
    }
  }
  str.sort();
  for (key = 0; key < str.length; key++) {
    sorted[str[key]] = encodeURIComponent(obj[str[key]]).replace(/%20/g, "+");
  }
  return sorted;
}

module.exports = { createPayment };
