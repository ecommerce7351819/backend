const User = require("../models/userModel");
const Product = require("../models/productModel");
const Cart = require("../models/cartModel");
const asyncHandler = require("express-async-handler");
const { generateToken } = require("../config/jwtToken");
const { generateRefreshToken } = require("../config/refreshToken");
const validateMongoDbId = require("../utils/validateMongodbId");
const jwt = require("jsonwebtoken");
const { sendEmail } = require("./emailController");
const crypto = require("crypto");
const uniqid = require("uniqid");
const { faker } = require("@faker-js/faker");

// Create a user
const createUser = asyncHandler(async (req, res) => {
  const email = req.body.email;
  const findUser = await User.findOne({ email: email });
  if (!findUser) {
    //Create a new user
    const user = await User.create(req.body);
    await Cart.create({
      products: [],
      cartTotal: 0,
      totalAfterDiscount: 0,
      orderby: user._id,
      quantityTotal: 0,
    });
    res.json({
      message: "Success",
    });
  } else {
    throw new Error("User Already Exists");
  }
});

const fakeUser = asyncHandler(async (req, res) => {
  try {
    for (let i = 0; i < 100; i++) {
      const user = await User.create({
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
        email: faker.internet.email(),
        mobile: faker.phone.number(),
        password: "12345678",
      });
      await Cart.create({
        products: [],
        cartTotal: 0,
        totalAfterDiscount: 0,
        orderby: user._id,
        quantityTotal: 0,
      });
    }
    res.json({
      message: "Done",
    });
  } catch (error) {}
});

// Login
const loginUserController = asyncHandler(async (req, res) => {
  const { email, password } = req.body;
  const findUser = await User.findOne({ email });
  if (findUser && (await findUser.isPasswordMatched(password))) {
    const refreshToken = await generateRefreshToken(findUser?.id);
    await User.findByIdAndUpdate(
      findUser.id,
      {
        refreshToken: refreshToken,
      },
      { new: true }
    );
    res.cookie("refreshToken", refreshToken, {
      httpOnly: true,
      maxAge: 72 * 60 * 60 * 1000,
    });
    res.json({
      _id: findUser?._id,
      firstName: findUser?.firstName,
      lastName: findUser?.lastName,
      email: findUser?.email,
      mobile: findUser?.mobile,
      token: generateToken(findUser?._id),
    });
  } else {
    throw new Error("Invalid Credentials");
  }
});

// Login Admin
const loginAdmin = asyncHandler(async (req, res) => {
  const { email, password } = req.body;
  const findAdmin = await User.findOne({ email });
  if (findAdmin.role === "user") throw new Error("Not Authorised");
  if (findAdmin && (await findAdmin.isPasswordMatched(password))) {
    const refreshToken = await generateRefreshToken(findAdmin?.id);
    await User.findByIdAndUpdate(
      findAdmin.id,
      {
        refreshToken: refreshToken,
      },
      { new: true }
    );
    res.cookie("refreshToken", refreshToken, {
      httpOnly: true,
      maxAge: 72 * 60 * 60 * 1000,
    });
    res.json({
      _id: findAdmin?._id,
      firstName: findAdmin?.firstName,
      lastName: findAdmin?.lastName,
      email: findAdmin?.email,
      mobile: findAdmin?.mobile,
      token: generateToken(findAdmin?._id),
    });
  } else {
    throw new Error("Invalid Credentials");
  }
});

// Refresh token
const handleRefreshToken = asyncHandler(async (req, res) => {
  const cookie = req.cookies;
  if (!cookie?.refreshToken) throw new Error("No Refresh Token in Cookies");
  const refreshToken = cookie.refreshToken;
  const user = await User.findOne({ refreshToken });
  if (!user) throw new Error("No refresh token present in db or not matched");
  jwt.verify(refreshToken, process.env.JWT_SECRET, (err, decode) => {
    if (err || user.id !== decode.id) {
      throw new Error("There is something wrong with refresh token");
    }
    const accessToken = generateToken(user?._id);
    res.json({ accessToken });
  });
});

// Logout functionality
const logout = asyncHandler(async (req, res) => {
  const cookie = req.cookies;
  if (!cookie?.refreshToken) throw new Error("No Refresh Token in Cookies");
  const refreshToken = cookie.refreshToken;
  const user = await User.findOne({ refreshToken });
  if (!user) {
    res.clearCookie("refreshToken", {
      httpOnly: true,
      secure: true,
    });
    return res.sendStatus(204); //forbidden
  }
  await User.findOneAndUpdate(refreshToken, {
    refreshToken: "",
  });
  res.clearCookie("refreshToken", {
    httpOnly: true,
    secure: true,
  });
  res.sendStatus(204); //forbidden
});

const getUserByToken = asyncHandler(async (req, res) => {
  try {
    res.json(req.user);
  } catch (error) {
    throw new Error(error);
  }
});

const getAdminByToken = asyncHandler(async (req, res) => {
  try {
    res.json(req.user);
  } catch (error) {
    throw new Error(error);
  }
});

// Get all users
const getAllUser = asyncHandler(async (req, res) => {
  const page = parseInt(req.query.page) || 1;
  const size = parseInt(req.query.size) || 10;
  const skip = (page - 1) * size;

  try {
    const users = await User.find()
      .skip(skip)
      .limit(size)
      .sort({ createdAt: -1 });

    const count = await User.countDocuments({});
    res.json({ users, count });
  } catch (error) {
    throw new Error(error);
  }
});

//Get a single user
const getUser = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const getUser = await User.findById(id);
    res.json({
      getUser,
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Update a user
const updateUser = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { name, mobile } = req.body;
  const nameParts = name.split(" ");
  const parts = [nameParts[0], nameParts.slice(1).join(" ")];
  validateMongoDbId(_id);
  try {
    const updateUser = await User.findByIdAndUpdate(
      _id,
      {
        firstName: parts[0],
        lastName: parts[1],
        mobile: mobile,
      },
      {
        new: true,
      }
    );
    res.json({
      message: "Success",
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Delete a user
const deleteUser = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const deleteUser = await User.findByIdAndDelete(id);
    res.json({
      deleteUser,
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Block and unblock user
const blockUser = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    await User.findByIdAndUpdate(
      id,
      {
        isBlocked: true,
      },
      {
        new: true,
      }
    );
    res.json({
      message: "User Blocked",
    });
  } catch (error) {
    throw new Error(error);
  }
});

const unblockUser = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    await User.findByIdAndUpdate(
      id,
      {
        isBlocked: false,
      },
      {
        new: true,
      }
    );
    res.json({
      message: "User Unblocked",
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Update password
const updatePassword = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { password } = req.body;
  validateMongoDbId(_id);
  const user = await User.findById(_id);
  if (password) {
    user.password = password;
    await user.save();
    res.json({
      message: "Done",
    });
  } else {
    res.json({
      message: "Failed",
    });
  }
});

const forgotPasswordToken = asyncHandler(async (req, res) => {
  const { email } = req.body;
  const user = await User.findOne({ email });
  if (!user) throw new Error("User not found with this this email");
  try {
    const token = await user.createPasswordResetToken();
    await user.save();
    const resetURL = `Hi, Please follow this to reset Your Password. 
		This link is valid till 10 minutes from now. <a href='${process.env.LOCAL_URL}/user/reset-password/${token}'>
		Click here!</a>`;
    const data = {
      to: email,
      text: "Hey, User",
      subject: "Forgot Password Link",
      html: resetURL,
    };
    sendEmail(data);
    res.json(token);
  } catch (error) {
    throw new Error(error);
  }
});

const resetPassword = asyncHandler(async (req, res) => {
  const { password } = req.body;
  const { token } = req.params;
  const hashedToken = crypto.createHash("sha256").update(token).digest("hex");
  const user = await User.findOne({
    passwordResetToken: hashedToken,
    passwordResetExpires: { $gt: Date.now() },
  });
  if (!user) throw new Error("Token Expired, Please try again later");
  user.password = password;
  user.passwordResetToken = undefined;
  user.passwordResetExpires = undefined;
  await user.save();
  res.json(user);
});

//Get wishlist
const getWishlist = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  try {
    const data = await User.findById(_id).populate("wishlist");
    res.json(data.wishlist);
  } catch (error) {
    throw new Error(error);
  }
});

//Save user address
const saveAddress = asyncHandler(async (req, res, next) => {
  const { _id } = req.user;
  const { name, province, district, ward, address } = req.body;
  validateMongoDbId(_id);
  try {
    const updateAddress = await User.findByIdAndUpdate(
      _id,
      {
        $push: { address: { name, province, district, ward, address } },
      },
      {
        new: true,
      }
    );
    res.json(updateAddress);
  } catch (error) {
    throw new Error(error);
  }
});

//User Cart

const calculator = (arr) => {
  let total = 0;
  arr.forEach((element) => {
    total += element.price * element.quantity;
  });
  return total;
};

const addToCart = asyncHandler(async (req, res) => {
  const { name, prodId, color, price, number, slug } = req.body;
  const { _id } = req.user;
  validateMongoDbId(_id);
  try {
    let cart = await Cart.findOne({ orderby: _id });

    // Kiểm tra xem sản phẩm đã tồn tại trong giỏ hàng hay không
    const isProductExist = cart.products.some(
      (product) =>
        product.product.toString() === prodId && product.color === color
    );

    if (isProductExist) {
      // Nếu sản phẩm đã tồn tại, tăng số lượng sản phẩm
      cart.products.forEach((item) => {
        if (item.product.toString() === prodId && item.color === color) {
          if (number !== undefined) {
            item.quantity = number;
          } else {
            item.quantity += 1;
          }
        }

        if (item.quantity === 0) {
          // Xóa item có quantity bằng 0 khỏi mảng
          cart.products = cart.products.filter(
            (product) => product.quantity !== 0
          );
        }
      });
    } else {
      // Nếu sản phẩm chưa tồn tại, thêm sản phẩm mới vào giỏ hàng
      const newProd = {
        product: prodId,
        name,
        slug,
        color,
        price,
        quantity: 1,
      };
      cart.products.unshift(newProd);
    }
    let qt = 0;
    cart.products.forEach((item) => {
      qt += item.quantity;
    });
    cart.quantityTotal = qt;
    cart.cartTotal = calculator(cart.products);
    await cart.save();
    res.json(cart);
  } catch (error) {
    throw new Error(error);
  }
});

const getUserCart = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validateMongoDbId(_id);
  try {
    const cart = await Cart.findOne({ orderby: _id }).populate({
      path: "products.product",
      select: "_id title mainImg",
    });
    cart.cartTotal = calculator(cart.products);
    await cart.save();
    res.json(cart);
  } catch (error) {
    throw new Error(error);
  }
});

const emptyCart = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validateMongoDbId(_id);
  try {
    const cart = await Cart.findOneAndUpdate({ orderby: _id });
    cart.products = [];
    cart.cartTotal = 0;
    cart.quantityTotal = 0;
    await cart.save();
    res.json(cart);
  } catch (error) {
    throw new Error(error);
  }
});

//Apply coupon
const applyCoupon = asyncHandler(async (req, res) => {
  const { coupon } = req.body;
  const { _id } = req.user;
  validateMongoDbId(_id);
  const validCoupon = await Coupon.findOne({ name: coupon });
  if (validCoupon === null) {
    throw new Error(error);
  }
  const user = await User.findOne({ _id });
  let { cartTotal } = await Cart.findOne({
    orderby: user._id,
  }).populate("products.product");
  let totalAfterDiscount = (
    cartTotal -
    (cartTotal * validCoupon.discount) / 100
  ).toFixed(2);
  await Cart.findOneAndUpdate(
    { orderby: user._id },
    { totalAfterDiscount },
    { new: true }
  );
  res.json(totalAfterDiscount);
});

const changeRoleUser = asyncHandler(async (req, res) => {
  try {
    const { id } = req.params;
    const { name, mobile, role } = req.body;
    const nameParts = name.split(" ");
    const parts = [nameParts[0], nameParts.slice(1).join(" ")];
    await User.findByIdAndUpdate(id, {
      firstName: parts[0],
      lastName: parts[1],
      mobile: mobile,
      role: role,
    });
    res.json({
      message: "Done",
    });
  } catch (error) {}
});

module.exports = {
  createUser,
  loginUserController,
  getAllUser,
  getUser,
  deleteUser,
  updateUser,
  blockUser,
  unblockUser,
  handleRefreshToken,
  logout,
  updatePassword,
  forgotPasswordToken,
  resetPassword,
  loginAdmin,
  getWishlist,
  saveAddress,
  addToCart,
  getUserCart,
  emptyCart,
  applyCoupon,
  changeRoleUser,
  getAdminByToken,
  getUserByToken,
  fakeUser,
};
