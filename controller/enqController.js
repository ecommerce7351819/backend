const Enquiry = require('../models/enqModel');
const asyncHandle = require('express-async-handler');
const validateMongoDbId = require('../utils/validateMongodbId');

//Create Enquiry
const createEnquiry = asyncHandle(async (req, res) => {
	try {
		await Enquiry.create(req.body);
		res.json({
			message: "Success"
		})
	} catch (error) {
		throw new Error(error);
	}
});

//Update Enquiry
const updateEnquiry = asyncHandle(async (req, res) => {
	try {
		const { id } = req.params;
		validateMongoDbId(id);
		const update = await Enquiry.findByIdAndUpdate(id, req.body, {
			new: true,
		});
		res.json(update);
	} catch (error) {
		throw new Error(error);
	}
});

//Get all Enquiry
const getAllEnquiry = asyncHandle(async (req, res) => {
	try {
		const enquiry = await Enquiry.find();
		res.json(Enquiry);
	} catch (error) {
		throw new Error(error);
	}
});

//Get a Enquiry
const getEnquiry = asyncHandle(async (req, res) => {
	try {
		const { id } = req.params;
		validateMongoDbId(id);
		const enquiry = await Enquiry.findById(id);
		res.json(Enquiry);
	} catch (error) {
		throw new Error(error);
	}
});

//Delete Enquiry
const deleteEnquiry = asyncHandle(async (req, res) => {
	try {
		const { id } = req.params;
		validateMongoDbId(id);
		await Enquiry.findByIdAndDelete(id);
		res.json({
			message: "Success"
		});
	} catch (error) {
		throw new Error(error);
	}
});


module.exports = { createEnquiry, updateEnquiry, getAllEnquiry, getEnquiry, deleteEnquiry }