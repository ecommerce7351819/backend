const PorductCategory = require('../models/productCategoryModel');
const asyncHandle = require('express-async-handler');
const validateMongoDbId = require('../utils/validateMongodbId');
const {deleteImg} = require('./uploadController')


//Create category
const createCategory = asyncHandle(async (req, res) => {
	try {
		await PorductCategory.create(req.body);
		res.json({
			message: "Success"
		})
	} catch (error) {
		throw new Error(error);
	}
});

//Update category
const updateCategory = asyncHandle(async (req, res) => {
	try {
		const { id } = req.params;
		validateMongoDbId(id);
		const update = await PorductCategory.findByIdAndUpdate(id, req.body, {
			new: true,
		});
		res.json(update);
	} catch (error) {
		throw new Error(error);
	}
});

//Get all categories
const getAllCategories = asyncHandle(async (req, res) => {
	try {
		const categoies = await PorductCategory.find();
		res.json(categoies);
	} catch (error) {
		throw new Error(error);
	}
});

//Get a category
const getCategory = asyncHandle(async (req, res) => {
	try {
		const { id } = req.params;
		validateMongoDbId(id);
		const categoy = await PorductCategory.findById(id);
		res.json(categoy);
	} catch (error) {
		throw new Error(error);
	}
});

//Delete Category
const deleteCategory = asyncHandle(async (req, res) => {
	try {
		const { id } = req.params;
		validateMongoDbId(id);
		const cate = await PorductCategory.findById(id);
		await deleteImg(cate.img.public_id)
		await PorductCategory.findByIdAndDelete(id);
		res.json({
			message: "Success"
		});
	} catch (error) {
		throw new Error(error);
	}
});


module.exports = { createCategory, updateCategory, getAllCategories, getCategory, deleteCategory }