const Brand = require('../models/brandModel');
const asyncHandle = require('express-async-handler');
const validateMongoDbId = require('../utils/validateMongodbId');
const {deleteImg} = require('./uploadController')

//Create brand
const createBrand = asyncHandle(async (req, res) => {
	try {
		await Brand.create(req.body);
		res.json({
			message: "Success"
		})
	} catch (error) {
		throw new Error(error);
	}
});

//Update brand
const updateBrand = asyncHandle(async (req, res) => {
	try {
		const { id } = req.params;
		validateMongoDbId(id);
		const update = await Brand.findByIdAndUpdate(id, req.body, {
			new: true,
		});
		res.json(update);
	} catch (error) {
		throw new Error(error);
	}
});

//Get all brand
const getAllBrand = asyncHandle(async (req, res) => {
	try {
		const brands = await Brand.find();
		res.json(brands);
	} catch (error) {
		throw new Error(error);
	}
});

//Get a brand
const getBrand = asyncHandle(async (req, res) => {
	try {
		const { id } = req.params;
		validateMongoDbId(id);
		const brand = await Brand.findById(id);
		res.json(brand);
	} catch (error) {
		throw new Error(error);
	}
});

//Delete brand
const deleteBrand = asyncHandle(async (req, res) => {
	try {
		const { id } = req.params;
		validateMongoDbId(id);
		const brand = await Brand.findById(id);
		await deleteImg(brand.img.public_id)
		await Brand.findByIdAndDelete(id);
		res.json({
			message: "Success"
		});
	} catch (error) {
		throw new Error(error);
	}
});


module.exports = { createBrand, updateBrand, getAllBrand, getBrand, deleteBrand }