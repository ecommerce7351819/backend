const Banner = require("../models/bannerModel");
const asyncHandle = require("express-async-handler");
const validateMongoDbId = require("../utils/validateMongodbId");
const { deleteImg } = require("./uploadController");

//Create banner
const createBanner = asyncHandle(async (req, res) => {
  try {
    await Banner.create(req.body);
    res.json({
      message: "Success",
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Update banner
const updateBanner = asyncHandle(async (req, res) => {
  try {
    const { id } = req.params;
    validateMongoDbId(id);
    const update = await Banner.findByIdAndUpdate(id, req.body, {
      new: true,
    });
    res.json(update);
  } catch (error) {
    throw new Error(error);
  }
});

//Get all banner
const getAllBanner = asyncHandle(async (req, res) => {
  try {
    const banners = await Banner.find();
    res.json(banners);
  } catch (error) {
    throw new Error(error);
  }
});

//Get a banner
const getBanner = asyncHandle(async (req, res) => {
  try {
    const { id } = req.params;
    validateMongoDbId(id);
    const banner = await Banner.findById(id);
    res.json(banner);
  } catch (error) {
    throw new Error(error);
  }
});

//Delete banner
const deleteBanner = asyncHandle(async (req, res) => {
  try {
    const { id } = req.params;
    validateMongoDbId(id);
    const banner = await Banner.findById(id);
    await deleteImg(banner.img.public_id);
    await Banner.findByIdAndDelete(id);
    res.json({
      message: "Success",
    });
  } catch (error) {
    throw new Error(error);
  }
});

const getSomeBanners = asyncHandle(async (req, res) => {
  try {
    const banners = await Banner.find({ subBanner: false, isShow: true })
      .sort({ createdAt: -1 })
      .limit(5);
    res.json(banners);
  } catch (error) {
    throw new Error(error);
  }
});

const getSubBanners = asyncHandle(async (req, res) => {
  try {
    const banners = await Banner.find({ subBanner: true, isShow: true })
      .sort({ createdAt: -1 })
      .limit(3);
    res.json(banners);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  createBanner,
  updateBanner,
  getAllBanner,
  getBanner,
  deleteBanner,
  getSomeBanners,
  getSubBanners,
};
