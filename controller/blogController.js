const Blog = require("../models/blogModel");
const User = require("../models/userModel");
const asyncHandle = require("express-async-handler");
const validateMongoDbId = require("../utils/validateMongodbId");
const {
  cloudinaryUploadImg,
  cloudinaryDeleteImg,
} = require("../utils/cloudinary");

//Create Blog
const createBlog = asyncHandle(async (req, res) => {
  try {
    await Blog.create(req.body);
    res.json({
      message: "success",
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Update Blog
const updateBlog = asyncHandle(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const updateBlog = await Blog.findByIdAndUpdate(id, req.body, {
      new: true,
    });
    res.json({
      message: "success",
      updateBlog,
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Get a Blog
const getBlog = asyncHandle(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const blog = await Blog.findById(id).populate("likes").populate("dislikes");
    await Blog.findByIdAndUpdate(
      id,
      {
        $inc: { numViews: 1 },
      },
      {
        new: true,
      }
    );
    res.json(blog);
  } catch (error) {
    throw new Error(error);
  }
});

//Get all blogs
const getAllBlog = asyncHandle(async (req, res) => {
  try {
    const blogs = await Blog.find();
    res.json(blogs);
  } catch (error) {
    throw new Error(error);
  }
});

//Delete blog
const deleteBlog = asyncHandle(async (req, res) => {
  try {
    const { id } = req.body;
    validateMongoDbId(id);
    await Blog.findByIdAndDelete(id);
    res.json({
      message: "Done",
    });
  } catch (error) {
    throw new Error(error);
  }
});

//Like blog
const likeBlog = asyncHandle(async (req, res) => {
  const { blogId } = req.body;
  validateMongoDbId(blogId);
  //Find the blog
  const blog = await Blog.findById(blogId);
  //Find user
  const loginUser = req?.user?._id;
  //Find if the user has liked the blog
  const isLiked = blog?.isLiked;
  //Find if the user has disliked the blog
  const alreadyDisliked = blog?.dislikes?.find(
    (userId) => userId?.toString() === loginUser?.toString()
  );
  if (alreadyDisliked) {
    const blog = await Blog.findByIdAndUpdate(
      blogId,
      {
        $pull: { likes: loginUser },
        isDisliked: false,
      },
      {
        new: true,
      }
    );
    res.json(blog);
  }
  if (isLiked) {
    const blog = await Blog.findByIdAndUpdate(
      blogId,
      {
        $pull: { likes: loginUser },
        isLiked: false,
      },
      {
        new: true,
      }
    );
    res.json(blog);
  } else {
    const blog = await Blog.findByIdAndUpdate(
      blogId,
      {
        $push: { likes: loginUser },
        isLiked: true,
      },
      { new: true }
    );
    res.json(blog);
  }
});

//Dislike blog
const dislikeBlog = asyncHandle(async (req, res) => {
  const { blogId } = req.body;
  validateMongoDbId(blogId);
  //Find the blog
  const blog = await Blog.findById(blogId);
  //Find user
  const loginUser = req?.user?._id;
  //Find if the user has liked the blog
  const isDisliked = blog?.isDisliked;
  //Find if the user has disliked the blog
  const alreadyLiked = blog?.likes?.find(
    (userId) => userId?.toString() === loginUser?.toString()
  );
  console.log(isDisliked);
  if (alreadyLiked) {
    const blog = await Blog.findByIdAndUpdate(
      blogId,
      {
        $pull: { likes: loginUser },
        isLiked: false,
      },
      {
        new: true,
      }
    );
    res.json(blog);
  }
  if (isDisliked) {
    const blog = await Blog.findByIdAndUpdate(
      blogId,
      {
        $pull: { dislikes: loginUser },
        isDisliked: false,
      },
      {
        new: true,
      }
    );
    res.json(blog);
  } else {
    const blog = await Blog.findByIdAndUpdate(
      blogId,
      {
        $push: { dislikes: loginUser },
        isDisliked: true,
      },
      { new: true }
    );
    res.json(blog);
  }
});

//Upload images
const uploadBlogImages = asyncHandle(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const uploader = (path) => cloudinaryUploadImg(path, "images");
    const urls = [];
    const files = req.files;
    for (const file of files) {
      const { path } = file;
      const newpath = await uploadImages(path);
      urls.push(newpath);
    }
    const findBlog = await Blog.findByIdAndUpdate(
      id,
      {
        images: urls.map((file) => {
          return file;
        }),
      },
      {
        new: true,
      }
    );
    res.json(findBlog);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  createBlog,
  updateBlog,
  getBlog,
  getAllBlog,
  deleteBlog,
  likeBlog,
  dislikeBlog,
  uploadBlogImages,
};
