const Order = require("../models/orderModel");
const Statistic = require("../models/statistic");
const asyncHandle = require("express-async-handler");

//Create Statistic
const createStatistic = asyncHandle(async (req, res) => {
  try {
    const hienTai = new Date();
    const thangHienTai = hienTai.getMonth();
    const namHienTai = hienTai.getFullYear();
    const date = thangHienTai + 1 + "/" + namHienTai;
    const title = "Thống kê tháng " + date;
    const capital = 100000000;
    const orders = await Order.find({ orderStatus: "Completed" });
    let revenue = 0;
    orders.forEach((element) => {
      revenue += element?.cart?.cartTotal;
    });
    const profit = revenue - capital;
    const statistic = await Statistic.create({
      title,
      date,
      revenue,
      profit,
    });
    res.json(statistic);
  } catch (error) {
    throw new Error(error);
  }
});

const updateStatistic = asyncHandle(async (req, res) => {});

const getStatistic = asyncHandle(async (req, res) => {
  try {
    const statistic = await Statistic.find().sort({ createdAt: -1 }).limit(7);
    res.json(statistic);
  } catch (error) {
    throw new Error(error);
  }
});

const getStatisticByMonth = asyncHandle(async (req, res) => {
  try {
    const { date1, date2 } = req.body;
    const statistic1 = await Statistic.findOne({ date: date1 });
    const statistic2 = await Statistic.findOne({ date: date2 });
    res.json({ lastMonth: statistic1, thisMonth: statistic2 });
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  createStatistic,
  getStatistic,
  getStatisticByMonth,
  updateStatistic,
};
