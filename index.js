const bodyParser = require("body-parser");
const express = require("express");
const dbConnect = require("./config/dbConnect");
const { notFound, errorHandle } = require("./middlewares/errorHandle");
const app = express();
const dotenv = require("dotenv").config();
const PORT = process.env.PORT || 2608;
const authRouter = require("./routes/authRoute");
const productRouter = require("./routes/productRoute");
const blogRouter = require("./routes/blogRoute");
const categoryRouter = require("./routes/productCategoryRoute");
const brandRouter = require("./routes/brandRoute");
const couponRouter = require("./routes/couponRoute");
const enqRouter = require("./routes/enqRoute");
const uploadRouter = require("./routes/uploadRoute");
const bannerRouter = require("./routes/bannerRouter");
const orderRouter = require("./routes/orderRouter");
const statisticRouter = require("./routes/statisticRouter");
const checkout = require("./routes/paymentRoute");
const cookieParser = require("cookie-parser");
const morgan = require("morgan");
const http = require("http");
const { Server } = require("socket.io");
const cors = require("cors");

dbConnect();

const allowedOrigins = ["http://localhost:3000", "http://localhost:4000"]; // Thay đổi theo origin của bạn

app.use(
  cors({
    origin: function (origin, callback) {
      if (!origin || allowedOrigins.includes(origin)) {
        callback(null, true);
      } else {
        callback(new Error("Not allowed by CORS"));
      }
    },
  })
);

const server = http.createServer(app);

const io = new Server(server, {
  cors: {
    origin: "http://localhost:3000",
  },
});

io.on("connection", (socket) => {
  console.log("New client connected " + socket.id);

  socket.on("join-room", (userId) => {
    socket.join(userId); // Tham gia vào room có ID là userId
    console.log(`User ${userId} joined room ${userId}`);
  });

  socket.on("change-cart", function () {
    io.emit("cart-update");
  });

  socket.on("disconnect", () => {
    console.log("Client disconnected");
  });
});

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET,HEAD,PUT,PATCH,POST,DELETE, OPTIONS"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

app.use("/api/user", authRouter);
app.use("/api/product", productRouter);
app.use("/api/blog", blogRouter);
app.use("/api/category", categoryRouter);
app.use("/api/brand", brandRouter);
app.use("/api/coupon", couponRouter);
app.use("/api/enquiry", enqRouter);
app.use("/api/upload", uploadRouter);
app.use("/api/banner", bannerRouter);
app.use("/api/order", orderRouter);
app.use("/api/statistic", statisticRouter);
app.use("/api/pay", checkout);

app.use(notFound);
app.use(errorHandle);

server.listen(PORT, () => {
  console.log(`Server is running at PORT ${PORT}`);
});
