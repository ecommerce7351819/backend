const express = require("express");
const {
  createProduct,
  getProduct,
  getAllProducts,
  updateProduct,
  deleteProduct,
  addToWishlist,
  rating,
  getHotProducts,
} = require("../controller/productController");
const { authMiddleware, isEmployee } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/create", authMiddleware, isEmployee, createProduct);

router.get("/get-product/:slug", getProduct);
router.get("/", getAllProducts);
router.get("/hot-product/", getHotProducts);

router.put("/update/:id", authMiddleware, isEmployee, updateProduct);
router.put("/wishlist", authMiddleware, addToWishlist);
router.put("/rating", authMiddleware, rating);

router.delete("/:id", authMiddleware, isEmployee, deleteProduct);

module.exports = router;
