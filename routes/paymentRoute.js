const express = require("express");
const { authMiddleware, isEmployee } = require("../middlewares/authMiddleware");
const { createPayment } = require("../controller/checkoutController");
const router = express.Router();

router.post("/create-payment-url", authMiddleware, createPayment);

module.exports = router;
