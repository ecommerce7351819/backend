const express = require("express");
const { authMiddleware, isEmployee } = require("../middlewares/authMiddleware");
const {
  createOrder,
  getOrdersByUserId,
  getAllOrder,
  changeOrderStatus,
  changeOrderPay,
} = require("../controller/orderController");
const router = express.Router();

router.post("/create", authMiddleware, createOrder);
router.get("/orders/:id?", authMiddleware, getOrdersByUserId);
router.get("/list", authMiddleware, getAllOrder);
router.put("/change-status/:id", authMiddleware, changeOrderStatus);
router.put("/change-pay/:id", authMiddleware, changeOrderPay);

module.exports = router;
