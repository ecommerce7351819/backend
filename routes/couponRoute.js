const express = require("express");
const {
  createCoupon,
  getAllCoupons,
  updateCoupon,
  deleteCoupon,
  checkCoupon,
} = require("../controller/couponController");
const { authMiddleware, isEmployee } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/create", authMiddleware, isEmployee, createCoupon);
router.post("/check-coupon", checkCoupon);

router.get("/list", getAllCoupons);
router.get("/get-coupon/:id", getAllCoupons);

router.put("/update/:id", authMiddleware, isEmployee, updateCoupon);

router.delete("/delete/:id", authMiddleware, isEmployee, deleteCoupon);

module.exports = router;
