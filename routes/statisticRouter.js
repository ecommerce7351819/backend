const express = require("express");
const { authMiddleware, isEmployee } = require("../middlewares/authMiddleware");
const {
  createStatistic,
  getStatistic,
  getStatisticByMonth,
  updateStatistic,
} = require("../controller/statisticController");
const router = express.Router();

router.post("/create", createStatistic);
router.get("/get-all", authMiddleware, isEmployee, getStatistic);
router.post("/get-by-month", authMiddleware, isEmployee, getStatisticByMonth);
router.put("/", updateStatistic);

module.exports = router;
