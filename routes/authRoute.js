const express = require("express");
const {
  createUser,
  loginUserController,
  getAllUser,
  getUser,
  deleteUser,
  updateUser,
  blockUser,
  unblockUser,
  handleRefreshToken,
  logout,
  updatePassword,
  forgotPasswordToken,
  resetPassword,
  loginAdmin,
  getWishlist,
  saveAddress,
  addToCart,
  getUserCart,
  emptyCart,
  applyCoupon,
  // createOrder,
  // getOrder,
  // updateOrderStatus,
  getUserByToken,
  getAdminByToken,
  changeRoleUser,
  fakeUser,
} = require("../controller/userController");
const {
  authMiddleware,
  isEmployee,
  isAdmin,
} = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/register", createUser);
router.post("/login", loginUserController);
router.post("/admin-login", loginAdmin);
router.post("/forgot-password-token", forgotPasswordToken);
router.post("/cart/apply-coupon", authMiddleware, applyCoupon);
// router.post("/cart/cash-order", authMiddleware, createOrder);

router.get("/wishlist", authMiddleware, getWishlist);
router.get("/", authMiddleware, isEmployee, getAllUser);
router.post("/fake-user", fakeUser);
router.get("/this-user", authMiddleware, getUserByToken);
router.get("/this-admin", authMiddleware, isEmployee, getAdminByToken);
router.get("/get-user/:id", getUser);
router.get("/refresh", handleRefreshToken);
router.get("/logout", logout);
router.get("/cart", authMiddleware, getUserCart);
// router.get("/get-order", authMiddleware, getOrder);

router.put("/add-to-cart", authMiddleware, addToCart);
router.put("/update-user", authMiddleware, updateUser);
router.put("/change-role/:id", authMiddleware, isAdmin, changeRoleUser);
router.put("/save-address", authMiddleware, saveAddress);
router.put("/block-user/:id", authMiddleware, isEmployee, blockUser);
router.put("/unblock-user/:id", authMiddleware, isEmployee, unblockUser);
router.put("/change-password", authMiddleware, updatePassword);
router.put("/reset-password/:token", resetPassword);
// router.put("/update-order/:id", authMiddleware, isEmployee, updateOrderStatus);
router.put("/empty-cart", authMiddleware, emptyCart);

router.delete("/:id", authMiddleware, isEmployee, deleteUser);

module.exports = router;
