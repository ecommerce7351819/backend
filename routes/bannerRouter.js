const express = require("express");
const {
  createBanner,
  updateBanner,
  getAllBanner,
  getBanner,
  deleteBanner,
  getSomeBanners,
  getSubBanners,
} = require("../controller/bannerController");
const { authMiddleware, isEmployee } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/create", authMiddleware, isEmployee, createBanner);

router.put("/update/:id", authMiddleware, isEmployee, updateBanner);

router.get("/list", getAllBanner);
router.get("/get-banner/:id", getBanner);
router.get("/get-main-banners", getSomeBanners);
router.get("/get-sub-banners", getSubBanners);

router.delete("/:id", authMiddleware, isEmployee, deleteBanner);

module.exports = router;
