const express = require("express");
const {
  createEnquiry,
  updateEnquiry,
  getAllEnquiry,
  getEnquiry,
  deleteEnquiry,
} = require("../controller/enqController");
const { authMiddleware, isEmployee } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/create", createEnquiry);

router.put("/update/:id", authMiddleware, isEmployee, updateEnquiry);

router.get("/list", getAllEnquiry);
router.get("/get-enquiry/:id", getEnquiry);

router.delete("/:id", authMiddleware, isEmployee, deleteEnquiry);

module.exports = router;
