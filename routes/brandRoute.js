const express = require("express");
const {
  createBrand,
  updateBrand,
  getAllBrand,
  getBrand,
  deleteBrand,
} = require("../controller/brandController");
const { authMiddleware, isEmployee } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/create", authMiddleware, isEmployee, createBrand);

router.put("/update/:id", authMiddleware, isEmployee, updateBrand);

router.get("/list", getAllBrand);
router.get("/get-brand/:id", getBrand);

router.delete("/:id", authMiddleware, isEmployee, deleteBrand);

module.exports = router;
