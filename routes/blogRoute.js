const express = require("express");
const {
  createBlog,
  updateBlog,
  getBlog,
  getAllBlog,
  deleteBlog,
  likeBlog,
  dislikeBlog,
  uploadBlogImages,
} = require("../controller/blogController");
const { authMiddleware, isEmployee } = require("../middlewares/authMiddleware");
const { uploadPhoto, blogImgResize } = require("../middlewares/uploadImages");
const router = express.Router();

router.post("/create", authMiddleware, isEmployee, createBlog);

router.put("/update-blog/:id", authMiddleware, isEmployee, updateBlog);
router.put("/likes", authMiddleware, likeBlog);
router.put("/dislikes", authMiddleware, dislikeBlog);
router.put(
  "/upload/:id",
  authMiddleware,
  isEmployee,
  uploadPhoto.array("images", 2),
  blogImgResize,
  uploadBlogImages
);

router.get("/get-blog/:id", getBlog);
router.get("/list", getAllBlog);

router.delete("/:id", authMiddleware, isEmployee, deleteBlog);

module.exports = router;
