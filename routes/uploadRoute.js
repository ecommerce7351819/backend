const express = require("express");
const { authMiddleware, isEmployee } = require("../middlewares/authMiddleware");
const {
  uploadPhoto,
  productImgResize,
} = require("../middlewares/uploadImages");
const {
  uploadImages,
  deleteImages,
} = require("../controller/uploadController");
const router = express.Router();

router.post(
  "/",
  authMiddleware,
  isEmployee,
  uploadPhoto.array("images", 10),
  productImgResize,
  uploadImages
);
router.delete("/delete-img/:id", authMiddleware, isEmployee, deleteImages);

module.exports = router;
