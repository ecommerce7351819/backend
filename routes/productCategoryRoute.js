const express = require("express");
const {
  createCategory,
  updateCategory,
  getAllCategories,
  getCategory,
  deleteCategory,
} = require("../controller/productCategoryController");
const { authMiddleware, isEmployee } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/create", authMiddleware, isEmployee, createCategory);

router.put("/update/:id", authMiddleware, isEmployee, updateCategory);

router.get("/list", getAllCategories);
router.get("/get-category/:id", getCategory);

router.delete("/:id", authMiddleware, isEmployee, deleteCategory);

module.exports = router;
